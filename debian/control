Source: python-hpilo
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Emmanuel Arias <eamanu@debian.org>,
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-hpilo
Vcs-Git: https://salsa.debian.org/python-team/packages/python-hpilo.git
Homepage: https://github.com/seveas/python-hpilo
Rules-Requires-Root: no

Package: python3-hpilo
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-hpilo-doc
Description: HP iLO XML interface access from Python (Python 3)
 This module will make it easy for you to access the Integrated Lights Out
 management interface of your HP hardware. It supports RILOE II, iLO, iLO 2, iLO
 3 and iLO 4. It uses the XML interface or hponcfg to access and change the iLO.
 .
 This package contains the Python 3 version of hpilo.

Package: python-hpilo-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: HP iLO XML interface access from Python - Documentation
 This module will make it easy for you to access the Integrated Lights Out
 management interface of your HP hardware. It supports RILOE II, iLO, iLO 2, iLO
 3 and iLO 4. It uses the XML interface or hponcfg to access and change the iLO.
 .
 This package contains the documentation for hpilo Python module.
